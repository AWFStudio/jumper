﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
	public GameObject hero;
	public GameManager disk;

    public Vector3 origin;
    public Vector3 originUnreached;

    eBlockState _state = eBlockState.GAME;
    float _timer = 0.0f;
    float _fullTime = 0.0f;

    float _yPos;

    void Start()
    {
        _yPos = transform.position.y;
        Update();
    }

    public void SetState(eBlockState state)
    {
        _state = state;
        if (_state == eBlockState.SHOW)
        {
            _timer = _fullTime = Random.Range(1.0f, 2.0f);
            _yPos = transform.position.y;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (hero)
        {
            float p = (transform.position.y - hero.transform.position.y) / 5.0f;
            if (_state == eBlockState.SHOW)
            {
                if (_timer > 0)
                {
                    _timer -= Time.deltaTime;
                    p = _timer / _fullTime;
                    transform.localPosition = Vector3.Lerp(origin, originUnreached, p) + new Vector3(0.0f,  30.0f * p * p, 0.0f);
                }
                else
                {
                    SetState(eBlockState.GAME);
                }
            }
            else
            {
                transform.localPosition = Vector3.Lerp(origin, originUnreached, p);
            }

        	float dist = Vector3.Distance(hero.transform.position, transform.position);
        	if (dist < 0.75f)
        	{
        		HeroController script = hero.GetComponent<HeroController>();
        		if (script)
        		{
        			script.FlyBack();
        		}
        		if (disk)
        		{
        			disk.RollBack();
        		}
		        //gameObject.SetActive(false);
		        //gameApp.GetComponent<GameApp>().getCoin();
        	}
        }
    }
}
