﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    int[,] levelArchitecture;
    List<int[]> ledders;
    List<string> generatedLevel;

    int leddersCount;
    int ledderIndent;
    int levelIndent;
    int stairIndent;
    int pos;

    string path = "Assets/Resources/generated.txt";

    //потом перенести во входящее значение
    [SerializeField] int difficultyLevel;


    public List<string> GenerateLevel()
    {
        levelArchitecture = new int[5, 36];
        ledders = new List<int[]>();
        generatedLevel = new List<string>();

        //Debug.Log("difficultyLevel = " + difficultyLevel);

        //определяем, сколько будет лестниц
        if (difficultyLevel == 1)
        {
            leddersCount = 3;
        }
        else if (difficultyLevel == 2 || difficultyLevel == 3)
        {
            leddersCount = 2;
        }
        else if (difficultyLevel == 4 || difficultyLevel == 5)
        {
            leddersCount = 1;
        }

        //расстояние между лестницами
        ledderIndent = 36 / leddersCount;

        //расстояние до 1й лесницы
        if (difficultyLevel == 1)
        {
            levelIndent = 5;
        }
        else if (difficultyLevel == 2)
        {
            levelIndent = 4;
        }
        else if (difficultyLevel == 3)
        {
            levelIndent = 3;
        }
        else if (difficultyLevel == 4)
        {
            levelIndent = 2;
        }
        else if (difficultyLevel == 5)
        {
            levelIndent = 1;
        }

        //заполняем список лестниц
        for (int i = 0; i < leddersCount; i++)
        {
            ledders.Add(new int[5]);
            //заполняем каждую лестницу ступеньками (при этом, длина ступенек зависит от уровня сложности)
            for (int f = 0; f < 5; f++)
            {
                int stepLength = 0;

                if (difficultyLevel == 1)
                {
                    stepLength = Random.Range(3, 5);
                }
                else if (difficultyLevel == 2)
                {
                    stepLength = Random.Range(3, 7);
                }
                else if (difficultyLevel == 3)
                {
                    stepLength = Random.Range(3, 7);
                }
                else if (difficultyLevel == 4)
                {
                    stepLength = Random.Range(2, 8);
                }
                else if (difficultyLevel == 5)
                {
                    stepLength = Random.Range(1, 8);
                }
                ledders[i][f] = stepLength;
            }
        }

        //Debug.Log("ledders = " + ledders.Count);

        for (int i = 0; i < leddersCount; i++)//все лестницы
        {
            if (i == 0) //первая лестница
            {
                for (int f = 0; f < 5; f++)
                {
                    //Debug.Log("Stair " + f + " created. Stair length = " + ledders[i][f]);

                    if (f == 0) //нижняя ступенька
                    {
                        for (int s = 0; s < ledders[i][f]; s++) //пробегаемся по всем блокам в ступеньке
                        {
                            pos = s + levelIndent;
                            if (pos > 35)
                            {
                                while (pos > 35)
                                {
                                    pos -= 36;
                                    //Debug.Log("Pos = " + pos);
                                }
                            }

                            levelArchitecture[f, pos] = 1;
                            //Debug.Log("Block " + s + ": cтрока: " + f + ", место " + s);
                            stairIndent = pos + 1;
                            //Debug.Log(" stairIndent = " + stairIndent);
                        }

                    }
                    else //не нижняя ступенька
                    {
                        //теперь, при размещении блока, ставим его в конце предыдущей ступени
                        for (int s = 0; s < ledders[i][f]; s++)
                        {
                            pos = s + stairIndent;
                            //Debug.Log("NewPos = " + pos);
                            if (pos > 35)
                            {
                                while (pos > 35)
                                {
                                    pos -= 36;
                                    //Debug.Log("Pos = " + pos);
                                }
                            }
                            levelArchitecture[f, pos] = 1;

                            //Debug.Log("Block " + s + ": cтрока: " + f + ", место " + pos);
                        }
                        stairIndent = pos + 1;
                        //Debug.Log(" stairIndent = " + stairIndent);
                    }
                }
            }
            else //не первая лестница
            {
                for (int f = 0; f < 5; f++)
                {
                    //Debug.Log("Stair " + f + " created. Stair length = " + ledders[i][f]);

                    if (f == 0)
                    {
                        for (int s = 0; s < ledders[i][f]; s++)
                        {
                            pos = s + levelIndent + ledderIndent * i;
                            //Debug.Log("Pos = " + pos);
                            if (pos > 35)
                            {
                                while (pos > 35)
                                {
                                    pos -= 36;
                                    //Debug.Log("Pos = " + pos);
                                }
                            }
                            levelArchitecture[f, pos] = 1;
                            //Debug.Log("Block " + s + ": cтрока: " + f + ", место " + pos);
                        }
                        stairIndent = pos + 1;
                        //Debug.Log(" stairIndent = " + stairIndent);
                    }
                    else
                    {
                        for (int s = 0; s < ledders[i][f]; s++)
                        {
                            pos = s + stairIndent;
                            //Debug.Log("Pos = " + pos);
                            if (pos > 35)
                            {
                                while (pos > 35)
                                {
                                    pos -= 36;
                                    //Debug.Log("Pos = " + pos);
                                }
                            }
                            levelArchitecture[f, pos] = 1;
                            //Debug.Log("Block " + s + ": cтрока: " + f + ", место " + pos);
                        }
                        stairIndent = pos + 1;
                        //Debug.Log(" stairIndent = " + stairIndent);
                    }
                }
            }
        }

        //расставляем разрывы
        if (difficultyLevel > 2)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int f = 0; f < 36; f++)
                {
                    if (levelArchitecture[i, f] == 1) //если на этом месте есть ступенька
                    {
                        bool setHole = true;

                        //1й блок 1й ступени не может быть провалом
                        if (i == 0)
                        {
                            if (f > 1 && levelArchitecture[i, f - 1] == 0 && levelArchitecture[i, f - 2] == 0) { setHole = false; }
                            if (f == 1 && levelArchitecture[i, f - 1] == 0 && levelArchitecture[i, 35] == 0) { setHole = false; }
                            if (f == 0 && levelArchitecture[i, 35] == 0 && levelArchitecture[i, 34] == 0) { setHole = false; }
                        }

                        //если последний блок предыдущей ступени был провалом, то 1й блок следующей не может быть провалом
                        if (i > 0)
                        {
                            if (f > 0 && levelArchitecture[i, f - 1] == 0 && levelArchitecture[i - 1, f - 1] == 0) { setHole = false; }
                            if (f == 0 && levelArchitecture[i, 35] == 0 && levelArchitecture[i - 1, 35] == 0) { setHole = false; }

                            //если уровень сложности меньше 4х, то если предпоследний блок предыдущей ступени был провалом, то 1й блок следующей не может быть провалом
                            if (difficultyLevel < 4)
                            {
                                if (f > 1 && levelArchitecture[i, f - 1] == 0 && levelArchitecture[i - 1, f - 2] == 0) { setHole = false; }
                                if (f == 1 && levelArchitecture[i, f - 1] == 0 && levelArchitecture[i - 1, 35] == 0) { setHole = false; }
                                if (f == 0 && levelArchitecture[i, 35] == 0 && levelArchitecture[i - 1, 34] == 0) { setHole = false; }
                            }
                        }

                        //не может идти два провала подряд
                        if (f > 1 && levelArchitecture[i, f - 1] == 0 && levelArchitecture[i, f - 2] == 1) { setHole = false; }
                        else if (f == 1 && levelArchitecture[i, f - 1] == 0 && levelArchitecture[i, 35] == 1) { setHole = false; }
                        else if (f == 0 && levelArchitecture[i, 35] == 0 && levelArchitecture[i, 34] == 1) { setHole = false; }

                        //перед провалом должно быть не меньше 2х кубиков
                        if (f > 1 && levelArchitecture[i, f - 1] == 1 && levelArchitecture[i, f - 2] == 0) { setHole = false; }
                        else if (f == 1 && levelArchitecture[i, f - 1] == 1 && levelArchitecture[i, 35] == 0) { setHole = false; }
                        else if (f == 0 && levelArchitecture[i, 35] == 1 && levelArchitecture[i, 34] == 0) { setHole = false; }

                        if (setHole)
                        {
                            int rand = Random.Range(0, 3);
                            if (rand == 1)
                            {
                                levelArchitecture[i, f] = 0;
                            }
                        }

                    }
                }
            }
        }

        //расставляем препятствия
        if (difficultyLevel > 1)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int f = 0; f < 36; f++)
                {
                    if (levelArchitecture[i, f] == 1) //если на этом месте есть ступенька
                    {
                        bool setObstacle = true;

                        //препятствие не может находиться на первом блоке ступени
                        if (f > 0 && levelArchitecture[i, f - 1] != 1) { setObstacle = false; }
                        else if (f == 0 && levelArchitecture[i, 35] != 1) { setObstacle = false; }

                        //препятствие не может находиться на втором блоке ступени
                        if (f > 1 && levelArchitecture[i, f - 1] == 1 && levelArchitecture[i, f - 2] != 1) { setObstacle = false; }
                        else if (f == 1 && levelArchitecture[i, f - 1] == 1 && levelArchitecture[i, 35] != 1) { setObstacle = false; }
                        else if (f == 0 && levelArchitecture[i, 35] == 1 && levelArchitecture[i, 34] != 1) { setObstacle = false; }

                        //препятствие не может находиться через 1 блок от предыдущего
                        if (f > 1 && levelArchitecture[i + 1, f - 2] == 3) { setObstacle = false; }
                        else if (f == 1 && levelArchitecture[i + 1, 35] == 3) { setObstacle = false; }
                        else if (f == 0 && levelArchitecture[i + 1, 34] == 3) { setObstacle = false; }

                        //препятствие не может находиться на предпоследнем блоке ступени
                        if (f < 34 && levelArchitecture[i, f + 1] == 1 && levelArchitecture[i, f + 2] == 0) { setObstacle = false; }
                        else if (f == 34 && levelArchitecture[i, f + 1] == 1 && levelArchitecture[i, 0] == 0) { setObstacle = false; }
                        else if (f == 35 && levelArchitecture[i, 0] == 1 && levelArchitecture[i, 1] == 0) { setObstacle = false; }

                        //препятствие не может находиться на последней ступени, если между этой ступенью и следующей есть провал
                        if (f < 35 && levelArchitecture[i, f + 1] == 0 && levelArchitecture[i + 1, f + 1] == 0) { setObstacle = false; }
                        else if (f == 35 && levelArchitecture[i, 0] == 0 && levelArchitecture[i + 1, 0] == 0) { setObstacle = false; }

                        //на уровне сложности меньше 3, препятствие не может находиться на последнем блоке ступени
                        if (difficultyLevel < 3)
                        {
                            if (f < 35 && levelArchitecture[i, f + 1] == 0) { setObstacle = false; }
                            else if (f == 35 && levelArchitecture[i, 0] == 0) { setObstacle = false; }
                        }

                        //на уровне сложности меньше 4, не может идти два препятствия подряд
                        if (difficultyLevel < 4)
                        {
                            if (f > 0 && levelArchitecture[i + 1, f - 1] == 3) { setObstacle = false; }
                            else if (f == 0 && levelArchitecture[i + 1, 35] == 3) { setObstacle = false; }
                        }

                        if (setObstacle)
                        {
                            int rand = Random.Range(0, 2);
                            if (rand == 1)
                            {
                                levelArchitecture[i + 1, f] = 3;
                            }
                        }
                    }
                }
            }
        }

        //расставляем звезды
        for (int i = 0; i < 4; i++)
        {
            for (int f = 0; f < 36; f++)
            {
                if (levelArchitecture[i, f] == 1)
                {
                    if (levelArchitecture[i + 1, f] != 3)
                    {
                        int rand = Random.Range(0, 2);
                        if (rand == 1)
                        {
                            levelArchitecture[i + 1, f] = 2;
                        }
                    }
                }
            }
        }

        //заполняем строки для переноса в текстовый документ
        for (int tear = 0; tear < 5; tear++)
        {
            generatedLevel.Add("");
            for (int i = 0; i < 36; i++)
            {
                if (levelArchitecture[tear, i] == 0)
                {
                    generatedLevel[tear] = generatedLevel[tear] + " ";
                }
                else if (levelArchitecture[tear, i] == 1)
                {
                    generatedLevel[tear] = generatedLevel[tear] + "1";
                }
                else if (levelArchitecture[tear, i] == 2)
                {
                    generatedLevel[tear] = generatedLevel[tear] + "2";
                }
                else if (levelArchitecture[tear, i] == 3)
                {
                    generatedLevel[tear] = generatedLevel[tear] + "3";
                }
            }
        }

        //дописываем последнюю строку
        generatedLevel.Add("");
        for (int i = 0; i < 36; i++)
        {
            generatedLevel[5] = generatedLevel[5] + "1";
        }

        generatedLevel.Reverse();

        //записываем строки в текстовый документ
        using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
        {
            sw.WriteLine(generatedLevel[0]);
            sw.WriteLine(generatedLevel[1]);
            sw.WriteLine(generatedLevel[2]);
            sw.WriteLine(generatedLevel[3]);
            sw.WriteLine(generatedLevel[4]);
            sw.WriteLine(generatedLevel[5]);
        }

        return generatedLevel;
    }


}
