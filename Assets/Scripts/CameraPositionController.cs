﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositionController : MonoBehaviour
{
	float _timer = 0.0f;
	Vector3 _targetPos;
	Vector3 _originPos;

    public void MoveUpTo(float height)
    {
        if (_timer <= 0.0f)
        {
        	_timer = 1.0f;
        	_originPos = transform.position;
        	_targetPos = new Vector3(_originPos.x, _originPos.y + height, _originPos.z);
        }
    }

    void Update()
    {
    	if (_timer > 0.0f)
    	{
    		_timer -= Time.deltaTime;
    		if (_timer <= 0.0f)
    		{
		        transform.position = _targetPos;
    		}
    		else
    		{
		        transform.position = Vector3.Lerp(_targetPos, _originPos, _timer);
    		}
    	}
    }
}
