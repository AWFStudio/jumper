﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Win Dialog")]
    [SerializeField] GameObject winDialogPanel;
    [SerializeField] Text winDialogScoreText;
    [SerializeField] Text winDialogHghScoreText;

    [Header("Lose Dialog")]
    [SerializeField] GameObject loseDialogPanel;
    [SerializeField] Text loseDialogScoreText;
    [SerializeField] Text loseDialogHghScoreText;
    [SerializeField] GameObject adsBttn;
    [SerializeField] GameObject restartBttn;

    [Header("Score")]
    [SerializeField] Text coinsCounter;
    [SerializeField] Animator coinsCountAC;
    static readonly int anim = Animator.StringToHash("anim");
    int score = 0;
    int highScore;

    SaveManager data;

    public GameObject AdsBttn { get => adsBttn; set => adsBttn = value; }
    public GameObject RestartBttn { get => restartBttn; set => restartBttn = value; }
    public int Coins { get => score; set => score = value; }

    private void Awake()
    {
        data = FindObjectOfType<SaveManager>();
    }

    public void ShowLoseDialog()
    {
        if (loseDialogPanel)
        {
            loseDialogPanel.SetActive(true);
            loseDialogScoreText.text = "SCORE: " + score.ToString();
            highScore = data.GetData("score");
            loseDialogHghScoreText.text = "HIGH SCORE: " + highScore.ToString();
            if (score > highScore)
            {
                data.CollectData("score", score);
            }
            data.CollectData("totalStars", score);
            data.Save();
            restartBttn.SetActive(false);
            adsBttn.SetActive(true);
        }
        else
        {
            Debug.Log("Lose window not found");
        }
    }

    public void ShowWinDialog()
    {
        if (winDialogPanel)
        {
            winDialogPanel.SetActive(true);
            winDialogScoreText.text = "SCORE: " + score.ToString();
            highScore = data.GetData("score");
            winDialogHghScoreText.text = "HIGH SCORE: " + highScore.ToString();
            if (score > highScore)
            {
                data.CollectData("score", score);
            }
            data.CollectData("totalStars", score);
            data.Save();
        }
        else
        {
            Debug.Log("Win window not found");
        }
    }

    public void RestartBttnPress()
    {
        Debug.Log("Restart");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void getCoin()
    {
        score++;
        coinsCounter.text = score.ToString();
        coinsCountAC.SetTrigger(anim);
    }


}
