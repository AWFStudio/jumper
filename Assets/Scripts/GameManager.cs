﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

// Add the possible states here
public enum eDiskState
{
    GAME,
    GAME_START_LOSE,
    LOSE,
    WIN,
    DESTROY_OLD_LEVEL,
    LOAD_NEW_LEVEL,
    BUILD_NEW_LEVEL
};

public class GameManager : MonoBehaviour
{
    [Header("Recourses")]
    [SerializeField] GameObject _cubePrefab;
    [SerializeField] GameObject _coinPrefab;
    [SerializeField] GameObject _dangerPrefab;
    [SerializeField] GameObject _timerPrefab;
    [SerializeField] List<TextAsset> levels_list;
    [SerializeField] TextAsset test_level;
    [SerializeField] Material objMat;
    [SerializeField] Color startColor;
    [SerializeField] Color finishColor;

    [Header("Debug")]
    [SerializeField] bool LevelGeneratorTest;

    //нигде не используются
    //float changeSpeed;
    //float _rowHeight = 1.4f;
    //float _cellStep = 1.5f * 1.4f;
    //float _radiusStep = 1.0f;

    //используются в этом и других скриптах
    float _maxRadius = 8.3f;
    float _stableLineHeight = 0.0f;
    float _baseHeight = 0.3f;
    float _levelHeight;
    List<GameObject> _stableLine = new List<GameObject>();
    List<GameObject> _gameObjects = new List<GameObject>();
    GameObject _hero;

    //используются только в этом скрипте
    int _lastDestroedGo = 0;
    const float time_for_level = 16.0f;
    float _rotateSpeed = 44.0f; // rotate speed in degrees
    float _rollBackStartSpeed = 0.0f;
    float _stateTimer;
    float _miniLevelTimer;
    eDiskState _state = eDiskState.GAME;
    List<GameObject> _timerDestroyObjects = new List<GameObject>();
    CameraPositionController myCamera;
    UIManager ui;
    SaveManager save;
    LevelConstructor builder;

    public float MaxRadius { get => _maxRadius; set => _maxRadius = value; }
    public float LevelHeight { get => _levelHeight; set => _levelHeight = value; }
    public float BaseHeight { get => _baseHeight; set => _baseHeight = value; }
    public GameObject CubePrefab { get => _cubePrefab; set => _cubePrefab = value; }
    public GameObject Hero { get => _hero; set => _hero = value; }
    public float StableLineHeight { get => _stableLineHeight; set => _stableLineHeight = value; }
    public GameObject CoinPrefab { get => _coinPrefab; set => _coinPrefab = value; }
    public GameObject DangerPrefab { get => _dangerPrefab; set => _dangerPrefab = value; }
    public List<GameObject> StableLine { get => _stableLine; set => _stableLine = value; }
    public List<GameObject> GameObjects { get => _gameObjects; set => _gameObjects = value; }

    private void Awake()
    {
        myCamera = FindObjectOfType<CameraPositionController>();
        ui = FindObjectOfType<UIManager>();
        save = FindObjectOfType<SaveManager>();
        builder = FindObjectOfType<LevelConstructor>();
        _hero = GameObject.FindGameObjectWithTag("Hero");
    }

    void Start()
    {
        // test_level.Load();
        // for (int i = 0; i < levels_list.Count; ++i)
        // {
        //     levels_list[i].Load();
        // }

        if (levels_list.Count > 0)
        {
            AddTimerObjects();
            StartRandomLevel(true);
        }

        SetState(eDiskState.GAME);
    }

    public void AddTimerObjects()
    {
        for (int i = 0; i < 10; ++i)
        {
            Vector3 origin = new Vector3(0.0f, _baseHeight + i - 0.5f, 0.0f);

            GameObject go = Instantiate(_timerPrefab);
            go.transform.parent = transform;
            go.transform.localPosition = origin;

            _timerDestroyObjects.Add(go);
        }
        _lastDestroedGo = 0;
    }

    void SetState(eDiskState state)
    {
        _state = state;
        if (_state == eDiskState.GAME)
        {
            _stateTimer = 0.0f;
            _miniLevelTimer = time_for_level;
        }
        else if (_state == eDiskState.DESTROY_OLD_LEVEL)
        {
            Vector3 zero = new Vector3();

            _stateTimer = 0.2f;
            for (int i = _lastDestroedGo; i < _timerDestroyObjects.Count; ++i)
            {
                Rigidbody rigidbody = _timerDestroyObjects[i].GetComponent<Rigidbody>();
                if (!rigidbody)
                {
                    rigidbody = _timerDestroyObjects[i].AddComponent<Rigidbody>();
                }
                BlockController script = _timerDestroyObjects[i].GetComponent<BlockController>();
                Vector3 pos = _timerDestroyObjects[i].transform.position;
                if (script)
                {
                    script.hero = null;
                }

                rigidbody.AddForce(Vector3.Lerp(zero, new Vector3(pos.x * 100, -100.0f, pos.z * 100), Random.Range(0.5f, 1.0f)));
                Destroy(_timerDestroyObjects[i], Random.Range(0.2f, 0.8f));
            }
            _lastDestroedGo = _timerDestroyObjects.Count;
            _timerDestroyObjects.Clear();

            DestroyLevelBlocks(true);

            //Debug.Log(levels_list.Count);

            if (levels_list.Count > 0)
            {
                myCamera.MoveUpTo(_levelHeight);

                AddTimerObjects();
            }
        }
        else if (_state == eDiskState.LOAD_NEW_LEVEL)
        {
            _gameObjects.Clear();
            //_timerDestroyObjects.Clear();

            _stateTimer = 0.1f;
            if (levels_list.Count > 1)
            {
                StartRandomLevel(false);
            }
            else if (levels_list.Count == 1)
            {
                builder.LoadLevel(levels_list[0], false);
                Debug.Log("LOAD_NEW_LEVEL");
                levels_list.Remove(levels_list[0]);
            }
        }
        else if (_state == eDiskState.GAME_START_LOSE)
        {
            _stateTimer = 1.5f;
        }
        else if (_state == eDiskState.LOSE)
        {
            DestroyLevelBlocks(false);
            save.CollectData("lose", 0);
            ui.ShowLoseDialog();
            Debug.Log("Lose");
        }
        else if (_state == eDiskState.WIN)
        {
            Debug.Log("Win");
            save.CollectData("win", 0);
            ui.ShowWinDialog();
        }
    }

    public void Shuffle(List<GameObject> gameObjects)
    {
        for (int i = 0; i < gameObjects.Count - 1; ++i)
        {
            int r = Random.Range(i + 1, gameObjects.Count);

            GameObject tmp = gameObjects[i];
            gameObjects[i] = gameObjects[r];
            gameObjects[r] = tmp;
        }
    }


    void DestroyLevelBlocks(bool win)
    {
        Vector3 zero = new Vector3();
        for (int i = 0; i < _gameObjects.Count; ++i)
        {
            if (_gameObjects[i] != null)
            {
                Rigidbody rigidbody = _gameObjects[i].AddComponent<Rigidbody>();
                BlockController script = _gameObjects[i].GetComponent<BlockController>();
                Vector3 pos = _gameObjects[i].transform.position;
                if (script)
                {
                    script.hero = null;
                }

                rigidbody.AddForce(Vector3.Lerp(zero, new Vector3(pos.x * 100, -100.0f, pos.z * 100), Random.Range(0.5f, 1.0f)));
                Destroy(_gameObjects[i], Random.Range(0.2f, 0.8f));
            }
        }
        _gameObjects.Clear();

        if (!win)
        {
            for (int i = 0; i < _stableLine.Count; ++i)
            {
                Rigidbody rigidbody = _stableLine[i].AddComponent<Rigidbody>();
                BlockController script = _stableLine[i].GetComponent<BlockController>();
                Vector3 pos = _stableLine[i].transform.position;
                if (script)
                {
                    script.hero = null;
                }

                rigidbody.AddForce(Vector3.Lerp(zero, new Vector3(pos.x * 100, -100.0f, pos.z * 100), Random.Range(0.5f, 1.0f)));
                Destroy(_stableLine[i], Random.Range(0.2f, 0.8f));
            }
            _stableLine.Clear();

            if (_hero)
            {
                Rigidbody rigidbody = _hero.AddComponent<Rigidbody>();
                Vector3 pos = _hero.transform.position;
                rigidbody.AddForce(new Vector3(pos.x * 1000, -100.0f, pos.z * 1000));
            }
        }
    }

    void StartRandomLevel(bool firstTime)
    {
        //if (test_level)
        //{
        //    builder.LoadLevel(test_level, firstTime);
        //    return;
        //}

        //Debug
        if (LevelGeneratorTest)
        {
            builder.GenerateLevel(firstTime);
            return;
        }

        int r = Random.Range(0, levels_list.Count - 1);
        builder.LoadLevel(levels_list[r], firstTime);
        levels_list.Remove(levels_list[r]);
    }

    public void UploadNextLevel()
    {
        //Debug.Log("UploadNextLevel");
        for (int i = 0; i < _stableLine.Count; ++i)
        {
            BlockController script = _stableLine[i].GetComponent<BlockController>();
            script.hero = null;
        }
        SetState(eDiskState.DESTROY_OLD_LEVEL);
    }

    public void RollBack()
    {
        _rollBackStartSpeed = 50.0f;
    }

    void Update()
    {
        float dt = Time.deltaTime;//* 0.1f;
        if (_state == eDiskState.GAME)
        {
            if (_hero.transform.position.y > _stableLineHeight)
            {
                UploadNextLevel();
            }
            //Debug.Log("0");
            if (_miniLevelTimer > 0.0f)
            {
                //Debug.Log("-");
                _miniLevelTimer -= dt;

                float p1 = (float)(_timerDestroyObjects.Count - _lastDestroedGo - 1) / _timerDestroyObjects.Count;
                if (p1 > _miniLevelTimer / time_for_level)
                {
                    //Debug.Log("2");
                    if (_lastDestroedGo < _timerDestroyObjects.Count)
                    {
                        //Debug.Log("3");
                        _timerDestroyObjects[_lastDestroedGo].AddComponent<Rigidbody>();
                        BlockController script = _timerDestroyObjects[_lastDestroedGo].GetComponent<BlockController>();
                        if (script)
                        {
                            script.hero = null;
                        }
                        Destroy(_timerDestroyObjects[_lastDestroedGo], 2.5f);
                        _lastDestroedGo++;
                    }
                }
                else if (_lastDestroedGo < _timerDestroyObjects.Count)
                {
                    Transform go = _timerDestroyObjects[_lastDestroedGo].transform.Find("Cylinder");
                    Material material = go.GetComponent<Renderer>().material;
                    float p0 = (_miniLevelTimer / time_for_level - p1) * 10;
                    material.color = Color32.Lerp(finishColor, startColor, p0);
                }

                if (_miniLevelTimer < 0.0f)
                {
                    SetState(eDiskState.LOSE);
                }
            }
        }
        else if (_state == eDiskState.GAME_START_LOSE)
        {
            _stateTimer -= dt;
            if (_stateTimer <= 0.0f)
            {
                SetState(eDiskState.LOSE);
            }
        }
        else if (_state == eDiskState.DESTROY_OLD_LEVEL)
        {
            _stateTimer -= dt;
            if (_stateTimer <= 0.0f)
            {
                if (levels_list.Count > 0)
                {
                    SetState(eDiskState.LOAD_NEW_LEVEL);
                }
                else
                {
                    SetState(eDiskState.WIN);
                }
            }
        }
        else if (_state == eDiskState.LOAD_NEW_LEVEL)
        {
            _stateTimer -= dt;
            if (_stateTimer <= 0.0f)
            {
                SetState(eDiskState.GAME);
            }
        }

        if (_rollBackStartSpeed > 0.0f)
        {
            _rollBackStartSpeed -= dt * 75;
            transform.Rotate(0.0f, -dt * _rollBackStartSpeed, 0.0f, Space.World);
        }
        else if (_state != eDiskState.LOSE)
        {
            transform.Rotate(0.0f, dt * _rotateSpeed, 0.0f, Space.World);
        }

        // for (int i = 0; i < _stableLine.Count; ++i)
        // {
        // 	//_stableLine[i].transform.position = new Vector3();
        // }
        // for (int i = 0; i < _gameObjects.Count; ++i)
        // {
        // 	//_gameObjects[i];
        // }

    }

    //не используются
    //public void DebugCreateBlock()
    //{
    //    float startLevelAngle = transform.eulerAngles.y / 180 * Mathf.PI;
    //    float angle = startLevelAngle;
    //    float height = _baseHeight;

    //    float x = _maxRadius * Mathf.Sin(angle);
    //    float z = -_maxRadius * Mathf.Cos(angle);
    //    Vector3 origin = new Vector3(x, height, z);

    //    Quaternion rotation = new Quaternion();
    //    rotation.eulerAngles = new Vector3(0, -angle / Mathf.PI * 180, 0);

    //    GameObject go = Instantiate(_cubePrefab);
    //    go.transform.parent = transform;
    //    go.transform.localPosition = origin;
    //    go.transform.localRotation = rotation;

    //    _gameObjects.Add(go);
    //}

    //void LoadLevel(TextAsset level, bool firstTime)
    //{
    //    if (level == null)
    //    {
    //        Debug.Log("Resource not found: levels.txt");
    //        return;
    //    }

    //    StreamReader reader = new StreamReader(new MemoryStream(level.bytes));

    //    List<string> level_lines = new List<string>();

    //    while (!reader.EndOfStream)
    //    {
    //        level_lines.Add(reader.ReadLine());
    //    }

    //    float minRadius = _maxRadius - _radiusStep * (level_lines.Count - 1);
    //    int line_length = level_lines[0].Length;

    //    float coin_radius = _maxRadius + _radiusStep;
    //    float coin_radius_unreached = minRadius + _radiusStep;

    //    _levelHeight = level_lines.Count * _rowHeight;

    //    float angleStep = 2.0f * Mathf.PI / (float)line_length;
    //    for (int i = 0; i < _stableLine.Count; ++i)
    //    {
    //        _gameObjects.Add(_stableLine[i]);
    //    }
    //    _stableLine.Clear();

    //    float startLevelAngle = transform.eulerAngles.y / 180 * Mathf.PI;

    //    if (firstTime)
    //    {
    //        Transform floor = transform.Find("floor");
    //        floor.GetComponent<MeshRenderer>().enabled = false;

    //        string line = level_lines[0];

    //        float height = _baseHeight;

    //        for (int i = 0; i < line.Length; ++i)
    //        {
    //            float angle = i * angleStep + startLevelAngle;

    //            float x = _maxRadius * Mathf.Sin(angle);
    //            float z = -_maxRadius * Mathf.Cos(angle);
    //            Vector3 origin = new Vector3(x, height, z);

    //            Quaternion rotation = new Quaternion();
    //            rotation.eulerAngles = new Vector3(0, -angle / Mathf.PI * 180, 0);

    //            GameObject go = Instantiate(_cubePrefab);
    //            go.transform.parent = transform;
    //            go.transform.localPosition = origin;
    //            go.transform.localRotation = rotation;

    //            _gameObjects.Add(go);
    //        }
    //    }
    //    else
    //    {
    //        startLevelAngle += 70.0f / 180.0f * Mathf.PI;
    //    }

    //    for (int l = 0; l < level_lines.Count; ++l)
    //    {
    //        string line = level_lines[l];

    //        float height = (level_lines.Count - l) * _rowHeight + _baseHeight;

    //        for (int i = 0; i < line.Length; ++i)
    //        {
    //            char c = line[i];
    //            if (c == ' ') continue;

    //            float angle = i * angleStep + startLevelAngle;

    //            float x = _maxRadius * Mathf.Sin(angle);
    //            float z = -_maxRadius * Mathf.Cos(angle);
    //            Vector3 origin = new Vector3(x, height, z);

    //            float xu = minRadius * Mathf.Sin(angle);
    //            float zu = -minRadius * Mathf.Cos(angle);
    //            Vector3 originUnreached = new Vector3(xu, height, zu);

    //            Quaternion rotation = new Quaternion();
    //            rotation.eulerAngles = new Vector3(0, -angle / Mathf.PI * 180, 0);

    //            if (c == '1')
    //            {
    //                GameObject go = Instantiate(_cubePrefab);
    //                go.transform.parent = transform;
    //                go.transform.localPosition = origin;
    //                go.transform.localRotation = rotation;

    //                block script = go.GetComponent<block>();

    //                script.origin = origin;
    //                script.originUnreached = originUnreached;
    //                script.hero = _hero;

    //                if (!firstTime)
    //                {
    //                    script.SetState(eBlockState.SHOW);
    //                }

    //                if (l == 0)
    //                {
    //                    _stableLine.Add(go);
    //                    _stableLineHeight = height - 0.1f;
    //                }
    //                else
    //                {
    //                    _gameObjects.Add(go);
    //                }
    //            }
    //            else if (c == '2')
    //            {
    //                GameObject go = Instantiate(_coinPrefab);
    //                go.transform.parent = transform;
    //                go.transform.localPosition = origin;
    //                go.transform.localRotation = rotation;
    //                coin_script script = go.GetComponent<coin_script>();
    //                script.Hero = _hero;
    //                script.Ui = ui;
    //                _gameObjects.Add(go);

    //                script.origin = origin;
    //                script.originUnreached = originUnreached;

    //                if (!firstTime)
    //                {
    //                    script.SetState(eBlockState.SHOW);
    //                }
    //            }
    //            else if (c == '3')
    //            {
    //                GameObject go = Instantiate(_dangerPrefab);
    //                go.transform.parent = transform;
    //                go.transform.localPosition = origin;
    //                go.transform.localRotation = rotation;
    //                Dander_script script = go.GetComponent<Dander_script>();
    //                script.hero = _hero;
    //                script.disk = this;
    //                // script.gameApp = gameApp;
    //                _gameObjects.Add(go);

    //                script.origin = origin;
    //                script.originUnreached = originUnreached;

    //                if (!firstTime)
    //                {
    //                    script.SetState(eBlockState.SHOW);
    //                }
    //            }
    //        }
    //    }
    //    reader.Close();

    //    _baseHeight += level_lines.Count * _rowHeight;

    //    Shuffle(_gameObjects);
    //}


}
