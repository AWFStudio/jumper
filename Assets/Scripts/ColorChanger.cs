﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
	public Material objMat;
	public Color startColor;
	public Color finishColor;
	public float changeSpeed;

	//bool colorChange = false;
	private float _blendValue;

	void Update()
	{
		_blendValue += Time.deltaTime * changeSpeed;
		objMat.color = Color32.Lerp(startColor, finishColor, _blendValue);
	}
}
