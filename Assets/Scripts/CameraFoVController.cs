﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFoVController : MonoBehaviour
{
    float horizontalFoV;
    Camera myCamera;

    void Start()
    {
        myCamera = GetComponent<Camera>();
        horizontalFoV = 26.0f;
    }

    void Update()
    {
        float halfWidth = Mathf.Tan(0.5f * horizontalFoV * Mathf.Deg2Rad);
        float halfHeight = halfWidth * Screen.height / Screen.width;
        float verticalFoV = 2.0f * Mathf.Atan(halfHeight) * Mathf.Rad2Deg;
        myCamera.fieldOfView = verticalFoV;
    }
}
