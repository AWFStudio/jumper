﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveManager : MonoBehaviour
{
    int highScore = 0;          //лучший результат
    int totalStars = 0;         //всего собрано звёзд за все игры
    //int currentStars;       //текущее количество звезд (все собранные - потраченные)
    int numberOfWins = 0;       //общее количество побед
    int numberOfDefeats = 0;    //общее количество поражений
    int viewedAds = 0;          //просмотрено рекламы без вознаграждения
    int viewedRewardedAds = 0;  //просмотрено рекламы с вознаграждением
    //int currentSkin;        //текущий выбранный скин
    //int adsDisabled;       //реклама отключена (дублируется проверкой через сторы)

    //List<bool> allSkins;    //все приобретенные игроком скины

    string data;
    string path;

    void Awake()
    {
        path = "diskrunnerData.txt";
    }

    private void Start()
    {
        if (!File.Exists(path))
        {
            File.Create(path);
        }
        else
        {
            Load();
        }

    }

    public int GetData(string type)
    {
        Load();

        if (type == "score")
        {
            return highScore;
        }
        else if (type == "totalStars")
        {
            return totalStars;
        }
        else
        {
            return 0;
        }
    }

    public void CollectData(string type, int value)
    {
        if (type == "lose")
        {
            numberOfDefeats++;
        }
        else if (type == "win")
        {
            numberOfWins++;
        }
        else if (type == "score")
        {
            highScore = value;
        }
        else if (type == "totalStars")
        {
            totalStars += value;
        }
        else if (type == "ads")
        {
            viewedAds++;
        }
        else if (type == "rewarded")
        {
            viewedRewardedAds++;
        }
    }

    public void Save()
    {
        data = JsonUtility.ToJson(this);
        File.WriteAllText(path, data);
    }

    void Load()
    {
        if (File.Exists(path))
        {
            data = File.ReadAllText(path);
            JsonUtility.FromJsonOverwrite(data, this);
        }
    }
}
