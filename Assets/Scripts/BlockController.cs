﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Add the possible states here
public enum eBlockState
{
  SHOW,
  GAME
};

public class BlockController : MonoBehaviour
{
	public GameObject hero;

	public Vector3 origin;
	public Vector3 originUnreached;

	Vector3 _scaleMin = new Vector3(0.4f, 1.0f, 1.0f);
	Vector3 _scaleMax = new Vector3(1.0f, 1.0f, 1.0f);

	eBlockState _state = eBlockState.GAME;
	float _timer = 0.0f;
	float _fullTime = 0.0f;

	float _yPos;

	public void SetState(eBlockState state)
	{
		_state = state;
		if (_state == eBlockState.SHOW)
		{
			_timer = _fullTime = Random.Range(1.0f, 2.0f);
			_yPos = transform.position.y;
		}
	}

	void Start()
	{
		_yPos = transform.position.y;
		Update();
	}

    void Update()
    {
        if (hero)
        {
			float p = (_yPos - hero.transform.position.y + 1.4f) / 5.0f;
	    	if (_state == eBlockState.SHOW)
	    	{
				if (_timer > 0)
	    		{
					_timer -= Time.deltaTime;
					transform.localPosition = Vector3.Lerp(origin, originUnreached, p) + new Vector3(0.0f,  30.0f * (_timer / _fullTime), 0.0f);

				}
				else
	    		{
	    			SetState(eBlockState.GAME);
				}
			}
	    	else
	    	{
				transform.localPosition = Vector3.Lerp(origin, originUnreached, p);
	    	}
			transform.localScale = Vector3.Lerp(_scaleMax, _scaleMin, p);
        }
    }
}
