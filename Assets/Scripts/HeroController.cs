﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Add the possible states here
public enum eCharacterState
{
    IDLE,
    RUN,
    JUMP_UP,
    FALL,
    ROLL_BACK
};

public class HeroController : MonoBehaviour
{
    public GameManager disk;

    float _ground;
    float _groundOrigin;
    float _zOrigin;
    float _touchTimer;
    float _gravity;
    float _verticalSpeed;
    float _stateTime; // time of current state(for junp - before we get highest poinf of jump)
    float _stateTimeCounter;
    float _angle;

    [SerializeField] AudioClip flyBackSound;
    [SerializeField] AudioClip pickUpSound;
    //[SerializeField] ParticleSystem pickUpParticles;

    eCharacterState _state = eCharacterState.IDLE;

    void Start()
    {
        disk = FindObjectOfType<GameManager>();
        _groundOrigin = _ground = transform.position.y;
        _zOrigin = transform.position.z;
        _gravity = 30.0f;
        _verticalSpeed = 0.0f;
        _stateTime = _stateTimeCounter = 0.0f;
        _angle = 0.0f;

        _state = eCharacterState.RUN;
    }

    void Update()
    {
        // decrement touch timer
        _touchTimer -= Time.deltaTime;
        _stateTimeCounter += Time.deltaTime;

        // handle mouse down or touch
        const float timeForTouch = 0.15f;
        if (Input.GetMouseButtonDown(0))
        {
            _touchTimer = timeForTouch;
        }
        else
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    _touchTimer = timeForTouch;
                    break;
                }
            }
        }

        // looking a ground under main hero
        RaycastHit hit1;
        if (Physics.Raycast(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), Vector3.down, out hit1, Mathf.Infinity))
        {
            //Debug.DrawRay(transform.position, Vector3.down * hit.distance, Color.yellow);
        }
        Debug.DrawRay(transform.position, Vector3.down * hit1.distance, Color.yellow);

        RaycastHit hit2;
        if (Physics.Raycast(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), Vector3.down, out hit2, Mathf.Infinity))
        {
            //Debug.DrawRay(transform.position, Vector3.down * hit.distance, Color.yellow);
        }
        Debug.DrawRay(transform.position, Vector3.down * hit2.distance, Color.yellow);

        float ground_distance = Mathf.Min(hit1.distance, hit2.distance);
        if (ground_distance < 0.7f)
        {
            //Debug.Log("ground_distance = 10, " + ground_distance);
            ground_distance = 10;
        }
        _ground = transform.position.y - ground_distance + 0.75f;

        // calc new position.y of hero
        float newY = transform.position.y + _verticalSpeed * Time.deltaTime - _gravity * Time.deltaTime * Time.deltaTime / 2.0f;
        _verticalSpeed -= _gravity * Time.deltaTime;
        if (_verticalSpeed < 0.0f && newY < _ground)
        {
            newY = _ground;
            //Debug.Log("newY = _ground");
            _verticalSpeed = 0.0f;
        }

        // calc hero position.z by height of hero
        // float height = transform.position.y - _groundOrigin;
        // if (height < 0.0f)
        // {
        //     height = 0.0f;    
        //     //Debug.Log("height = 0.0f");
        // }
        // float z = _zOrigin + (5 * 1.0f) * (height / (5 * 1.4f));// 5 rows, 1.4 - height of row, 1.0f - row's radius step

        Vector3 position = new Vector3(transform.position.x, newY, _zOrigin);//z);
        transform.position = position;

        // is touch and ground - jump!
        if (_touchTimer > 0.0f && Mathf.Abs(newY - _ground) < 1e-3f && _state == eCharacterState.RUN)
        {
            _stateTimeCounter = 0.0f;
            _touchTimer = 0.0f;
            _verticalSpeed = 10.0f;
            _stateTime = _verticalSpeed / _gravity;
            _state = eCharacterState.JUMP_UP;

            //debug|test
            if (disk)
            {
                //disk.DebugCreateBlock();
            }
        }

        if (_state == eCharacterState.JUMP_UP)
        {
            // rotate the body if needed (jump up)
            if (_stateTimeCounter < _stateTime)
            {
                float deltaAngle = -_stateTimeCounter / _stateTime * 90;
                transform.rotation = Quaternion.Euler(0.0f, 0.0f, _angle + deltaAngle);
            }
            else
            {
                _angle += -90;
                if (_angle < 0 || _angle > 360)
                {
                    _angle = _angle % 360;
                    _angle = Mathf.Round(_angle / 90) * 90;
                    //Debug.Log(_angle);
                }
                _state = eCharacterState.RUN;
                transform.rotation = Quaternion.Euler(0.0f, 0.0f, _angle);
            }
        }
        else if (_state == eCharacterState.ROLL_BACK)
        {
            if (_stateTimeCounter > _stateTime)
            {
                _state = eCharacterState.RUN;
                transform.rotation = Quaternion.Euler(0.0f, 0.0f, _angle);
            }
        }
    }

    public void FlyBack()
    {
        GetComponent<AudioSource>().PlayOneShot(flyBackSound);
        _state = eCharacterState.ROLL_BACK;
        _stateTimeCounter = 0.0f;
        _verticalSpeed = 7.0f;
        _stateTime = _verticalSpeed / _gravity;
        //Debug.Log("FlyBack");
    }

    public void PlayPickUpSound()
    {
        GetComponent<AudioSource>().PlayOneShot(pickUpSound);
    }

}
