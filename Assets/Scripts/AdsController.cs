﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class AdsController : MonoBehaviour, IUnityAdsListener
{
    enum AdsType
    {
        Ads,
        RewardedAds
    }

    [SerializeField] AdsType adsType;

    //d804afbc-4fdc-48d9-86ad-2197da792672

    string gameId;
    bool testMode = true; //тест мод включен
    Button bttn;
    UIManager ui;
    SaveManager data;

    //проверка отключения рекламы
    //IAPManager iAP;

    void Awake()
    {
        //проверка отключения рекламы
        //iAP = FindObjectOfType<IAPManager>();

#if UNITY_IOS
        gameId = "3841182"; // идентификатор приложения для IOS заменить на свой
#elif UNITY_ANDROID
        gameId = "3841183"; // идентификатор приложения для андроид заменить на свой
#endif

        ui = FindObjectOfType<UIManager>();
        bttn = gameObject.GetComponent<Button>();
        data = FindObjectOfType<SaveManager>();
    }

    private void Start()
    {
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize(gameId, testMode);
            bttn.interactable = Advertisement.IsReady("rewardedVideo");
            if (adsType == AdsType.RewardedAds)
            {
                if (bttn) bttn.onClick.AddListener(ShowRewardedAds);
            }
            Advertisement.AddListener(this);
        }
    }

    public void ShowAds()
    {
        //если не проплачено отключение рекламы
        //if (!iAP.CheckAdRemoval())
        //{
        if (Advertisement.IsReady())
        {
            StartCoroutine(Show());
            Debug.Log("show ads");
        }
        //}
    }

    public void ShowRewardedAds()
    {
        Advertisement.Show("rewardedVideo");
        Debug.Log("show rewarded ads");
    }

    IEnumerator Show()
    {
        Advertisement.Show("video");
        //music is off
        yield return null;
        while (Advertisement.isShowing) yield return null;
        //music is on
        data.CollectData("ads", 0);
        data.Save();
        ui.AdsBttn.SetActive(false);
        ui.RestartBttn.SetActive(true);
    }

    public void OnUnityAdsReady(string placementId)
    {
        if (placementId == "rewardedVideo")
        {
            bttn.interactable = true;
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.Log(" Unity Ads Error!");
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        //music is off
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        //music is on
        if (showResult == ShowResult.Finished)
        {
            //reward Player
            bttn.interactable = false;
            data.CollectData("rewarded", 0);
            data.Save();
        }
        else if (showResult == ShowResult.Skipped)
        {
            //do not reward Player
        }
        else if (showResult == ShowResult.Failed)
        {
            //do not reward Player
        }
        Advertisement.RemoveListener(this);
    }
}
