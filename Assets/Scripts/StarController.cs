﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarController : MonoBehaviour
{
    [SerializeField] GameObject star;
    [SerializeField] GameObject particles;
    GameObject hero;
    float _timer = 0.0f;
    float _fullTime = 0.0f;
    //float _yPos;
    bool isPicked = false;
    eBlockState _state = eBlockState.GAME;
    UIManager ui;
    Vector3 origin;
    Vector3 originUnreached;

    public GameObject Hero { get => hero; set => hero = value; }
    public UIManager Ui { get => ui; set => ui = value; }
    public Vector3 OriginUnreached { get => originUnreached; set => originUnreached = value; }
    public Vector3 Origin { get => origin; set => origin = value; }

    private void Awake()
    {
        ui = FindObjectOfType<UIManager>();
        //_yPos = transform.position.y;
    }

    public void SetState(eBlockState state)
    {
        _state = state;
        if (_state == eBlockState.SHOW)
        {
            _timer = _fullTime = Random.Range(1.0f, 2.0f);
            //_yPos = transform.position.y;
        }
    }

    void Update()
    {
        if (hero)
        {
            float p = (transform.position.y - hero.transform.position.y) / 5.0f;
            if (_state == eBlockState.SHOW)
            {
                if (_timer > 0)
                {
                    _timer -= Time.deltaTime;
                    transform.localPosition = Vector3.Lerp(Origin, OriginUnreached, p) + new Vector3(0.0f, 30.0f * (_timer / _fullTime), 0.0f);
                }
                else
                {
                    SetState(eBlockState.GAME);
                }
            }
            else
            {
                transform.localPosition = Vector3.Lerp(Origin, OriginUnreached, p);
            }

            float dist = Vector3.Distance(hero.transform.position, transform.position);
            if (dist < 1 && isPicked == false)
            {
                hero.GetComponent<HeroController>().PlayPickUpSound();
                isPicked = true;
                StartCoroutine(PickUp());
            }
        }
    }

    IEnumerator PickUp()
    {
        ui.getCoin();
        star.SetActive(false);
        particles.SetActive(true);
        particles.GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);

    }

}
