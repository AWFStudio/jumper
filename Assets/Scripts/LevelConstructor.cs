﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelConstructor : MonoBehaviour
{
    LevelGenerator gen;
    GameManager game;
    UIManager ui;

    //рассмотреть возможность вынести метод DestroyLevelBlocks из GameManager в отдельный класс
    //рассмотреть возможность вынести метод UploadNextLevel из GameManager в отдельный класс

    [SerializeField] GameObject baseConstruct;
    [SerializeField] GameObject _cubePrefab;
    [SerializeField] GameObject _coinPrefab;
    [SerializeField] GameObject _dangerPrefab;

    float _rowHeight = 1.4f;
    float _radiusStep = 1.0f;

    void Awake()
    {
        gen = FindObjectOfType<LevelGenerator>();
        game = FindObjectOfType<GameManager>();
        ui = FindObjectOfType<UIManager>();
    }

    public void LoadLevel(TextAsset level, bool firstTime)
    {
        Debug.Log("LoadLevel");

        if (level == null)
        {
            Debug.Log("Resource not found: levels.txt");
            return;
        }

        StreamReader reader = new StreamReader(new MemoryStream(level.bytes));
        List<string> level_lines = new List<string>();

        while (!reader.EndOfStream)
        {
            level_lines.Add(reader.ReadLine());
        }
        BuildLevel(level_lines, firstTime);
        reader.Close();
    }

    public void GenerateLevel(bool firstTime)
    {
        List<string> level_lines = new List<string>();
        level_lines.AddRange(gen.GenerateLevel());
        BuildLevel(level_lines, firstTime);
    }

    public void BuildLevel(List<string> level_lines, bool firstTime)
    {
        float minRadius = game.MaxRadius - _radiusStep * (level_lines.Count - 1);
        int line_length = level_lines[0].Length;

        float coin_radius = game.MaxRadius + _radiusStep;
        float coin_radius_unreached = minRadius + _radiusStep;

        game.LevelHeight = level_lines.Count * _rowHeight;

        float angleStep = 2.0f * Mathf.PI / (float)line_length;

        for (int i = 0; i < game.StableLine.Count; ++i)
        {
            game.GameObjects.Add(game.StableLine[i]);
        }
        game.StableLine.Clear();

        float startLevelAngle = transform.eulerAngles.y / 180 * Mathf.PI;

        if (firstTime)
        {
            Transform floor = transform.Find("floor");
            floor.GetComponent<MeshRenderer>().enabled = false;

            string line = level_lines[0];
            float height = game.BaseHeight;
            for (int i = 0; i < line.Length; ++i)
            {
                float angle = i * angleStep + startLevelAngle;

                float x = game.MaxRadius * Mathf.Sin(angle);
                float z = -game.MaxRadius * Mathf.Cos(angle);
                Vector3 origin = new Vector3(x, height, z);

                Quaternion rotation = new Quaternion();
                rotation.eulerAngles = new Vector3(0, -angle / Mathf.PI * 180, 0);

                GameObject go = Instantiate(_cubePrefab);
                go.transform.parent = transform;
                go.transform.localPosition = origin;
                go.transform.localRotation = rotation;

                game.GameObjects.Add(go);
            }
        }
        else
        {
            startLevelAngle += 70.0f / 180.0f * Mathf.PI;
        }

        for (int l = 0; l < level_lines.Count; ++l)
        {
            string line = level_lines[l];
            float height = (level_lines.Count - l) * _rowHeight + game.BaseHeight;
            for (int i = 0; i < line.Length; ++i)
            {
                char c = line[i];
                if (c == ' ') continue;
                float angle = i * angleStep + startLevelAngle;

                float x = game.MaxRadius * Mathf.Sin(angle);
                float z = -game.MaxRadius * Mathf.Cos(angle);
                Vector3 origin = new Vector3(x, height, z);

                float xu = minRadius * Mathf.Sin(angle);
                float zu = -minRadius * Mathf.Cos(angle);
                Vector3 originUnreached = new Vector3(xu, height, zu);

                Quaternion rotation = new Quaternion();
                rotation.eulerAngles = new Vector3(0, -angle / Mathf.PI * 180, 0);

                if (c == '1')
                {
                    GameObject go = Instantiate(_cubePrefab);
                    go.transform.parent = transform;
                    go.transform.localPosition = origin;
                    go.transform.localRotation = rotation;

                    BlockController script = go.GetComponent<BlockController>();

                    script.origin = origin;
                    script.originUnreached = originUnreached;
                    script.hero = game.Hero;

                    if (!firstTime)
                    {
                        script.SetState(eBlockState.SHOW);
                    }

                    if (l == 0)
                    {
                        game.StableLine.Add(go);
                        game.StableLineHeight = height - 0.1f;
                    }
                    else
                    {
                        game.GameObjects.Add(go);
                    }
                }
                else if (c == '2')
                {
                    GameObject go = Instantiate(_coinPrefab);
                    go.transform.parent = transform;
                    go.transform.localPosition = origin;
                    go.transform.localRotation = rotation;
                    StarController script = go.GetComponent<StarController>();
                    script.Hero = game.Hero;
                    script.Ui = ui;
                    game.GameObjects.Add(go);

                    script.Origin = origin;
                    script.OriginUnreached = originUnreached;

                    if (!firstTime)
                    {
                        script.SetState(eBlockState.SHOW);
                    }
                }
                else if (c == '3')
                {
                    GameObject go = Instantiate(_dangerPrefab);
                    go.transform.parent = transform;
                    go.transform.localPosition = origin;
                    go.transform.localRotation = rotation;
                    ObstacleController script = go.GetComponent<ObstacleController>();
                    script.hero = game.Hero;
                    script.disk = game;
                    // script.gameApp = gameApp;
                    game.GameObjects.Add(go);

                    script.origin = origin;
                    script.originUnreached = originUnreached;

                    if (!firstTime)
                    {
                        script.SetState(eBlockState.SHOW);
                    }
                }
            }
        }

        game.BaseHeight += level_lines.Count * _rowHeight;
        game.Shuffle(game.GameObjects);
    }



}
