﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] Text highScoreText;
    [SerializeField] Text coinsAmountText;

    SaveManager data;
    private void Awake()
    {
        data = FindObjectOfType<SaveManager>();
    }
    private void Start()
    {
        coinsAmountText.text = data.GetData("totalStars").ToString();
        highScoreText.text = "HIGH SCORE: " + data.GetData("score").ToString();
    }

    public void PlayBttnPress()
    {
        Debug.Log("Restart");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
